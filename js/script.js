let allSpanes = document.querySelectorAll(".parent .buttons span");

let results = document.querySelector(".results >span");

let theInput = document.getElementById("the-input");

allSpanes.forEach(span =>{
    span.addEventListener("click",(e) => {

        if(e.target.classList.contains("check-item")){
            checkItem();
        }

        
        if(e.target.classList.contains("Add-item")){
            addItem();
        }

        
        if(e.target.classList.contains("delete-item")){
            deleteItem();
        }

        
        if(e.target.classList.contains("Show-item")){
            showItem();
        }
    });
});



function showMessage(){
        results.innerHTML = 'Input Cant Be Empty';
}


function checkItem(){
    //console.log("work");
    if (theInput.value !== ''){
        
        if(localStorage.getItem(theInput.value)){
            results.innerHTML = `Found Local Item Called <span>${theInput.value}<span>`;
        }else{
            results.innerHTML = `no Local Storage Item with The Name <span>${theInput.value}<span>`;
        }

    }else{

        showMessage();

    }
    
}
function addItem(){
    if (theInput.value !== ''){
        
       localStorage.setItem(theInput.value,"test");

       results.innerHTML = `Local Storage Item <span>${theInput.value} </span> Added`;

       theInput.value = "";

    }else{

        showMessage();

    }
}
function deleteItem(){
    if (theInput.value !== ''){
        
        if(localStorage.getItem(theInput.value)){
            localStorage.removeItem(theInput.value);
            results.innerHTML = `Local Item Deleted <span>${theInput.value}<span>`;
        }else{
            results.innerHTML = `no Local Storage Item with The Name <span>${theInput.value}<span>`;
        }
 
     }else{
 
         showMessage();
 
     }
}
function showItem(){
    if(localStorage.length){
        //console.log(`Found Element ${localStorage.length}`);
        results.innerHTML = "";
        for(let [key,value] of Object.entries(localStorage)){
            results.innerHTML += `<span class="result">${key} : ${value}</span>`;
        }
    }else{
        results.innerHTML = 'Local Storage Is Empty';
    }
}